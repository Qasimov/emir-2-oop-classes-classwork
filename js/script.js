// let User= {
//     name : this.name,
//     age : this.age,
//     role : this.role
// };
//
// let Admin={
//     role : "admin",
//     changeContent:true
// };
//
// let admin=Admin.__proto__;
// let user=User.__proto__;
//
// admin.__proto__=user;
// admin.age

// /**
//  *
//  * @constructor
//  * @param size        size of the hamburger
//  * @param stuffing    selected stuffing
//  * @throws {HamburgerException}  In case of incorrect usage
//  */
// function Hamburger(size, stuffing) {
//   var toppings = [];
//   this.size = size;
//   this.stuffing = stuffing;
//   this.setToppings = (newToppings) => {
//     if (!newToppings instanceof Array) {
//       throw new Error("new toppings has to be Array")
//     }
//     /* deleting all old values from toppings and inserting new values */
//     toppings.splice(
//       0,
//       toppings.length,
//       ...newToppings
//     )
//   };
//   this.getToppings = () => {
//     // return toppings;
//     return [...toppings];
//     // return Array.copy(toppings);
//   }
// }
//
// /* Sizes, types of stuffings and toppings */
// Hamburger.SIZE_SMALL = Object.freeze({
//   price: 50,
//   cal: 20
// });
//
// Hamburger.SIZE_LARGE = {
//   price: 100,
//   cal: 20
// };
// Hamburger.STUFFING_CHEESE = {
//   price: 10,
//   cal: 20
// };
// Hamburger.STUFFING_SALAD = {
//   price: 20,
//   cal: 5
// };
// Hamburger.STUFFING_POTATO = {
//   price: 15,
//   cal: 10
// };
// Hamburger.TOPPING_MAYO = {
//   price: 20,
//   cal: 5
// };
// Hamburger.TOPPING_SPICE = {
//   price: 15,
//   cal: 0
// };
//
// /**
//  * Add topping to hamburger. Several toppings can be added, only if they are different. You can't add same topping two times.
//  *
//  * @param topping     type of topping will be passed as a String
//  * @throws {HamburgerException}  in case of incorrect usage
//  */
// Hamburger.prototype.addTopping = function (topping) {
//   try {
//     var toppingChecked = this.checkTopping(topping);
//     var currentToppings = this.getToppings();
//
//     if (currentToppings.some(el => el === toppingChecked)) {
//       throw new HamburgerException("This topping already exists!")
//     } else {
//       currentToppings.push(toppingChecked);
//       this.setToppings(currentToppings);
//     }
//   } catch (e) {
//     console.error(e.message);
//   }
// };
//
// Hamburger.prototype.checkTopping = function (topping) {
//   switch (topping) {
//     case "TOPPING_MAYO":
//       return Hamburger["TOPPING_MAYO"];
//     case "TOPPING_SPICE":
//       return Hamburger["TOPPING_SPICE"];
//     default:
//       throw new HamburgerException("Incorrect topping value!");
//   }
// };
//
// /**
//  * Delete the toppping, only if it was added earlier.
//  *
//  * @param topping   topping type
//  * @throws {HamburgerException}  in case of incorrect usage
//  */
// Hamburger.prototype.removeTopping = function (topping) {
//   try {
//     var toppingChecked = this.checkTopping(topping);
//     var currentToppings = this.getToppings();
//
//     if (!currentToppings.some(el => el === toppingChecked)) {
//       throw new HamburgerException("Nothing to delete")
//     } else {
//       currentToppings.splice(currentToppings.indexOf(topping),1);
//       this.setToppings(currentToppings);
//     }
//   } catch (e) {
//     console.error(e.message);
//   }
// };
//
// /**
//  * Get list of toppings
//  *
//  * @return {Array} an Array with the list of constants like Hamburger.TOPPING_* inside
//  */
// // Hamburger.prototype.getToppings = function () ...
//
// /**
//  * Find out the size of the Hamburger
//  */
// // Hamburger.prototype.getSize = function () ...
//
// /**
//  * Find out the stuffing of the Hamburger
//  */
// // Hamburger.prototype.getStuffing = function () ...
//
// /**
//  * Find out the price of the hamburger
//  * @return {Number} the number of price in AZN
//  */
// // Hamburger.prototype.calculatePrice = function () ...
//
// /**
//  * Find out callories amount of the hamburger
//  * @return {Number} Number of calories
//  */
//
// // Hamburger.prototype.calculateCalories = function () ...
//
// /**
//  * Provides information about an error while working with a hamburger.
//  * Details are stored in the message property.
//  * @constructor
//  */
// function HamburgerException(msg) {
//   this.message = msg;
// }
//
// // HamburgerException.prototype = Object.create(Error.prototype);
//
// const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
//
// console.log("toppings before");
// console.table(hamburger.getToppings());
//
// hamburger.addTopping("TOPPING_MAYO");
//
// console.log("toppings after");
// console.table(hamburger.getToppings());
//
// hamburger.addTopping("TOPPING_MAYO");

class Machine{
    constructor(...allArg){
        const [name, date, garantyTerm] = allArg; // destructor

        this.name = name || new Error("Unnamed !");
        this.date = new Date(date).toDateString() || new Error("Date is not correct");
        this.garantyTerm = garantyTerm || "NO Garanty";
    }

    turnOn(){
        return console.log("returned from Machine");
    }
}


class coffeeMachine extends Machine{
    constructor(arg, supplies, drinks){
        super(arg.name, arg.date, arg.garantyTerm);
        this.supplies = supplies;
        this.drinks = drinks;
    }

    turnOn() {
        return console.log(`turned on in coffeeMachine`);
    }

    getDrink(drinkName){
        return this.name = drinkName;
    }
}

const machine = new Machine("Tea","10/03/15", "sjdhjdhgdshgdshgfdjhg");
console.log(machine);

const coffeemachine =  new coffeeMachine(machine ,"salty","juice");

console.log(coffeemachine);
console.log(coffeemachine.getDrink("PEPSI"));

console.log(coffeemachine.turnOn());

